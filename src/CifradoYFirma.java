// CifradoYFirma.java
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

public class CifradoYFirma {
    public static byte[] cifrarMensaje(String mensaje) throws Exception {
        byte[] publicKeyBytes = Files.readAllBytes(Paths.get("publicKey"));
        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKeyBytes));
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(mensaje.getBytes());
    }

    public static byte[] firmarMensaje(String mensaje) throws Exception {
        byte[] privateKeyBytes = Files.readAllBytes(Paths.get("privateKey"));
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        Signature firma = Signature.getInstance("SHA256withRSA");
        firma.initSign(privateKey);
        firma.update(mensaje.getBytes());
        return firma.sign();
    }

    public static void guardarMensajeCifrado(String fileName, byte[] data) throws Exception {
        try (FileOutputStream out = new FileOutputStream(fileName)) {
            out.write(data);
        }
    }

    public static void guardarFirma(String fileName, byte[] data) throws Exception {
        try (FileOutputStream out = new FileOutputStream(fileName)) {
            out.write(data);
        }
    }
}
