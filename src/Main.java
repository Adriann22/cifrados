import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            GeneradorClaves.generarClaves();
            Scanner scanner = new Scanner(System.in);
            System.out.println("mensaje a cifrar: ");
            String mensaje = scanner.nextLine();
            scanner.close();

            byte[] mensajeCifrado = CifradoYFirma.cifrarMensaje(mensaje);
            byte[] firma = CifradoYFirma.firmarMensaje(mensaje);

            CifradoYFirma.guardarMensajeCifrado("mensajeCifrado", mensajeCifrado);
            CifradoYFirma.guardarFirma("firmaMensaje", firma);
            System.out.println("cifrado y firma generados");

            String mensajeDescifrado = DescifrarYFirma.descifrarMensaje("mensajeCifrado");
            boolean firmaVerificada = DescifrarYFirma.verificarFirma(mensajeDescifrado, "firmaMensaje");

            System.out.println("Mensaje descifrado: " + mensajeDescifrado);
            System.out.println("Firma verificada: " + firmaVerificada);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
