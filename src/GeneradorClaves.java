import java.io.FileOutputStream;
import java.security.*;

public class GeneradorClaves {
    public static void generarClaves() throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
        keyPairGen.initialize(2048);
        KeyPair keyPair = keyPairGen.generateKeyPair();
        guardarArchivo("publicKey", keyPair.getPublic().getEncoded());
        guardarArchivo("privateKey", keyPair.getPrivate().getEncoded());
        System.out.println("Claves generadas y guardadas");
    }

    private static void guardarArchivo(String fileName, byte[] data) throws Exception {
        try (FileOutputStream out = new FileOutputStream(fileName)) {
            out.write(data);
        }
    }
}