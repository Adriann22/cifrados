import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

public class DescifrarYFirma {
    public static String descifrarMensaje(String fileName) throws Exception {
        byte[] privateKeyBytes = Files.readAllBytes(Paths.get("privateKey"));
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
        byte[] mensajeCifrado = Files.readAllBytes(Paths.get(fileName));
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] mensajeDescifrado = cipher.doFinal(mensajeCifrado);
        return new String(mensajeDescifrado);
    }

    public static boolean verificarFirma(String mensaje, String fileName) throws Exception {
        byte[] publicKeyBytes = Files.readAllBytes(Paths.get("publicKey"));
        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicKeyBytes));
        byte[] firmaBytes = Files.readAllBytes(Paths.get(fileName));
        Signature firma = Signature.getInstance("SHA256withRSA");
        firma.initVerify(publicKey);
        firma.update(mensaje.getBytes());
        return firma.verify(firmaBytes);
    }
}
